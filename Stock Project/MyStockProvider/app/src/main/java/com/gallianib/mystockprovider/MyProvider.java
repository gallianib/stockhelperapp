package com.gallianib.mystockprovider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;


public class MyProvider extends ContentProvider {

    private static final String DATABASE_NAME = "provider_db";
    private static final String TABLE_NAME = "stocklist";
    private static final int DATABASE_VERSION = 1;
    private SQLiteDatabase db;
    private static final String CREATE_DB_TABLE =
            "CREATE TABLE stocklist (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "symbol TEXT,name TEXT,date TEXT,time TEXT,last REAL,change REAL);";

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_DB_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME + ";");
            onCreate(db);
        }
    }

    static final String PROVIDER = "com.gallianib.mystockprovider.MyProvider";
    static final String URL = "content://"+PROVIDER+"/stocklist";
    static final Uri CONTENT_URI = Uri.parse(URL);

    static final String symbol ="symbol";
    static final String name ="name";
    static final String time ="time";
    static final String date ="date";
    static final String change ="change";
    static final String last ="last";
    static final int SYMBOL = 1;
    static final int NAME = 2;

    static final UriMatcher uriMatcher;
    static{
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER, "stocklist", SYMBOL);
        uriMatcher.addURI(PROVIDER, "stocklist/#", NAME);
    }
    @Override
    public int delete(Uri uri,String sel,String[]args){return 0;}
    @Override
    public int update(Uri uri, ContentValues val,String sel,String[]args){return 0;}
    @Override
    public String getType(Uri uri){return null;}

    @Override
    public boolean onCreate(){
        Context context = getContext();
        DatabaseHelper dbh = new DatabaseHelper(context);
        db = dbh.getWritableDatabase();
        return(db!=null?true:false);
    }

    @Override
    public Uri insert(Uri uri,ContentValues values){

        long rowID = db.insert("stocklist",null,values);
        if(rowID > 0) {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URI,rowID);
            getContext().getContentResolver().notifyChange(_uri,null);
            return _uri;
        }
        throw new SQLException("Failed to add a record into "+ uri);
    }

    @Override
    public Cursor query(Uri uri,String[] projection,String sel,String[]args,String sortOrder){
        Cursor cursor = db.query("stocklist",projection,sel,args,null,null,sortOrder);
        getContext().getContentResolver().notifyChange(uri,null);
        return cursor;
    }
}

