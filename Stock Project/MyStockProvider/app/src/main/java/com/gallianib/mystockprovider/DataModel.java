package com.gallianib.mystockprovider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.xmlpull.v1.XmlPullParser;

import android.util.Xml;

public class DataModel {

    static String baseUrl = "http://www.webservicex.net/stockquote.asmx/GetQuote?symbol=";
    String symbol="";
    double last;
    double change;
    String name="";
    String time="";
    String date="";


    public DataModel(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
    public double getLast() {
        return last;
    }
    public void setLast(double last) {
        this.last = last;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }
    public double getChange() {
        return change;
    }
    public void setChange(double change) {
        this.change = change;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }


    public void getURLData() throws IOException {

        String urlSpec = baseUrl + symbol;
        URL url = new URL(urlSpec);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        try {
            InputStream instream = connection.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
            String innerXml = new String();
            XmlPullParser parser = Xml.newPullParser();

            // auto-detect the encoding from the stream
            parser.setInput(instream, "UTF-8");
            int eventType = parser.getEventType();

            //boolean done = false;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    innerXml=parser.nextText();
                }
                eventType = parser.next();
            }

            parser=Xml.newPullParser();
            parser.setInput(new StringReader(innerXml));

            eventType=parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {

                if (eventType == XmlPullParser.START_TAG) {
                    String name = parser.getName();

                    if (name.equalsIgnoreCase("last")) {
                        this.setLast(Double.parseDouble(parser.nextText()));
                    } else if (name.equalsIgnoreCase("date")) {
                        this.setDate(parser.nextText());
                    } else if (name.equalsIgnoreCase("name")) {
                        this.setName((parser.nextText()));
                    } else if (name.equalsIgnoreCase("time")) {
                        this.setTime((parser.nextText()));
                    } else if (name.equalsIgnoreCase("change")) {
                        this.setChange(Double.parseDouble(parser.nextText()));
                    }
                }

                eventType = parser.next();
            }

            instream.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }

    }
}

