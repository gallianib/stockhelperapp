package com.gallianib.mystockprovider;

import android.content.ContentValues;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.in;

public class MainActivity extends AppCompatActivity {
    DataModel mStock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickAddWatchStock(View view) {
        String symbol = ((EditText) findViewById(R.id.symbol)).getText().toString();

        mStock = new DataModel(symbol);
        new MyTask().execute(mStock);
    }

    class MyTask extends AsyncTask<DataModel, Void, DataModel> {

        @Override
        protected DataModel doInBackground(DataModel... params) {
            try {
                params[0].getURLData();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return params[0];
        }

        @Override
        protected void onPostExecute(DataModel result) {

            if (result.getTime().equals(""))
                Toast.makeText(MainActivity.this, "Stock Symbol not found", Toast.LENGTH_SHORT).show();
            else {
                ContentValues values = new ContentValues();
                values.put(MyProvider.symbol, result.getSymbol());
                values.put(MyProvider.name, result.getName());
                values.put(MyProvider.date, result.getDate());
                values.put(MyProvider.time, result.getTime());
                values.put(MyProvider.last, result.getLast());
                values.put(MyProvider.change, result.getChange());
                Uri uri = getContentResolver().insert(MyProvider.CONTENT_URI, values);

                String str = ""+result.getName()+"\'s stock price is " + Double.toString(result.getLast()) + " at " + result.getTime();
                Toast.makeText(MainActivity.this, str, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

