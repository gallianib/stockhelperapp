package com.gallianib.provideruser;

import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<Cursor> {

    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<DataModel> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        getSupportLoaderManager().initLoader(0, null, this);
    }
    @Override
    public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {

        CursorLoader cursorLoader = new CursorLoader(this,
                Uri.parse("content://com.gallianib.mystockprovider.MyProvider/stocklist"), null, null, null, null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor) {
        data = new ArrayList<DataModel>();
        cursor.moveToFirst();
        while(!(cursor.isAfterLast())){
            DataModel d = new DataModel(cursor.getString(cursor.getColumnIndex("symbol")));
            d.setName(cursor.getString(cursor.getColumnIndex("name")));
            d.setDate(cursor.getString(cursor.getColumnIndex("date")));
            d.setTime(cursor.getString(cursor.getColumnIndex("time")));
            d.setLast(cursor.getFloat(cursor.getColumnIndex("last")));
            d.setChange(cursor.getFloat(cursor.getColumnIndex("change")));
            data.add(d);
            cursor.moveToNext();
        }
        adapter = new CustomAdapter(data);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> arg0) {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
