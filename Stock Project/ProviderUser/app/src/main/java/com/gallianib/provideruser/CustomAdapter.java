package com.gallianib.provideruser;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {

   public static class ViewHolder extends RecyclerView.ViewHolder {
       TextView symbol;
       TextView last;
       TextView change;
       TextView name;
       TextView time;
       TextView date;

       public ViewHolder(View itemView){
           super(itemView);
           symbol = (TextView)itemView.findViewById(R.id.symbol);
           last = (TextView)itemView.findViewById(R.id.total);
           change = (TextView)itemView.findViewById(R.id.change);
           name = (TextView)itemView.findViewById(R.id.name);
           time = (TextView)itemView.findViewById(R.id.time);
           date = (TextView)itemView.findViewById(R.id.date);
       }
   }
   private ArrayList<DataModel> dataset;
    public CustomAdapter(ArrayList<DataModel> data){dataset=data;}
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cards_layout,parent,false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder,final int listPosition){
        TextView symbol = holder.symbol;
        TextView last = holder.last;
        TextView change = holder.change;
        TextView name = holder.name;
        TextView time = holder.time;
        TextView date = holder.date;

        symbol.setText(dataset.get(listPosition).getSymbol());
        last.setText(String.format("%.2f",dataset.get(listPosition).getLast()));
        change.setText(String.format("%.2f",dataset.get(listPosition).getChange()));
        name.setText(dataset.get(listPosition).getName());
        time.setText(dataset.get(listPosition).getTime());
        date.setText(dataset.get(listPosition).getDate());
        symbol.setTextColor(Color.BLUE);
        name.setTextColor(Color.BLACK);
        last.setTextColor(Color.BLACK);
        if (dataset.get(listPosition).getChange() >= 0){
            change.setTextColor(Color.GREEN);
        }else if (dataset.get(listPosition).getChange() < 0){
            change.setTextColor(Color.RED);
        }
    }
    @Override
    public int getItemCount(){
        return dataset.size();
    }
}
